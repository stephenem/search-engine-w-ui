@extends('search/index')

@section('navbar')
    <ul class="nav navbar-nav">
        <li>
            <a href="#">About</a>
        </li>
    </ul>
@stop

@section('page_content')
    <div class="col-lg-12 text-center">
        <div id="custom-search-input">
            {!! Form::open() !!}
            <div class="input-group col-md-12">
                <input name="query" type="text" class=" search-query form-control" placeholder="Search" />
                            <span class="input-group-btn">
                                <button class="btn btn-danger" type="submit">
                                    <span class="glyphicon glyphicon-search"></span>
                                </button>
                            </span>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop